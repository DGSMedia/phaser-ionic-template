import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { ToastProvider } from '../providers/toast/toast';
import {IonicStorageModule} from '@ionic/storage';

import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from '@angular/http';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { GameScreen } from '../pages/game-screen/game-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    GameScreen
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    GameScreen
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthenticationProvider,
    ToastProvider
    
  ]
})
export class AppModule {}
