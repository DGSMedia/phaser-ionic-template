import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthenticationProvider {
  apiUrl = "http://localhost:3333/api/";
  data: any;

  constructor(private http: HttpClient, private storage: Storage) {  }

  login(user) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    let body = JSON.stringify({ username: user.username, password: user.password });
    return new Promise(resolve => {
      this.http.post(this.apiUrl + "signin", body, { headers }).subscribe((data: any) => {
        if (data && !data.error) this.storage.set('token', data.content);
        resolve(data);
      });
    })
  }

  logout() {
    let message = "You have been successfully logged out!";
    return new Promise(resolve => {
      this.storage.remove('token').then(val => {
        resolve({ error: false, content: message });
      });
    })
  }

  validateToken() {
    let body = { adminToken: null };
    let url = "http://localhost:3333/api/check";
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return new Promise(resolve => {
      this.storage.get("token").then((token: string) => {
        if (!token) return resolve({ error: true, content: "no token" });
        body.adminToken = token;
        this.http.post(url, JSON.stringify(body), { headers }).subscribe((data: any) => {
          if (data && !data.error) resolve({error:false});
          else this.logout();
        });
      })
    })
  }

}