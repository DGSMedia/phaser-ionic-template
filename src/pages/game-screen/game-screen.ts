import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { App }  from '../../game/game';

@Component({
  selector: 'page-game-screen',
  templateUrl: 'game-screen.html',
})
export class GameScreen {
  public gameInstance: App;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	
  }

  ionViewDidLoad() {
    this.startApp()
  }
  
  startApp(): void {
    let gameWidth: number = function () { return Math.max(window.innerWidth, document.documentElement.clientWidth); }();
    let gameHeight: number = function () { return Math.max(window.innerHeight, document.documentElement.clientHeight); }();
    console.log(gameWidth, gameHeight);
    
    
    // There are a few more options you can set if needed, just take a look at Phaser.IGameConfig
    let gameConfig: Phaser.IGameConfig = {
        width: gameWidth,
        height: gameHeight,
        renderer: Phaser.CANVAS,
        parent: 'game',
        resolution: 1
    };
    this.gameInstance = new App(gameConfig);
    //return app;
  }
}
