import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { ToastProvider } from '../../providers/toast/toast';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  
  user = {
    username:"stefano",
    password:"asd123"
  }
  
  constructor(public navCtrl: NavController, private API: AuthenticationProvider,public toastCtrl: ToastProvider) {

  }

  login(user) {
    this.API.login(user).then((res:any) => {
      if (res && res.error) return this.toastCtrl.show('wrong credentials!', 3000);
      this.toastCtrl.show("You have been successfully log in!", 3000);
      this.checkToken();
    });
  }

  logout() {
    this.API.logout().then((res:any) => {
      if (res && !res.error) this.toastCtrl.show(res.content, 3000);
      this.checkToken();
    });
  }

  checkToken() {
    this.API.validateToken().then((res:any) => {
      if (res && res.error) return this.toastCtrl.show("Invalid Token", 3000);
      else this.navCtrl.push(HomePage);
    });
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }
  fakeLogin(){
    this.navCtrl.push(HomePage);
  }
}