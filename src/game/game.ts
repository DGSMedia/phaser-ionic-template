
// import pixi, p2 and phaser ce
import "pixi";
import "p2";
import * as Phaser from "phaser-ce";


import  Boot from '../game/states/Boot';
import  Preload from '../game/states/Preload';
import Main from '../game/states/Main';


export class App extends Phaser.Game {

    constructor(config: Phaser.IGameConfig) {
        super (config);
        // add some game states
        this.state.add('Boot', Boot);
        this.state.add('Preload', Preload);
        this.state.add('Main', Main);
        // start with boot state
        this.state.start('Boot');
    }
}