
export default class Preload extends Phaser.State {
  preloadBar: any;
  ready = false;
 
  preload() {
  
    this.preloadBar = this.add.sprite(10, 30, 'preloadbar');
    this.load.setPreloadSprite(this.preloadBar);
    this.load.image('j', '/assets/imgs/btn.png');
  
  
    /* game ui */
    /*
    this.load.image('compass', 'assets/gui/touchBase.png');
    this.load.image('touch', 'assets/gui/touch.png');
    this.load.image('j', 'assets/gui/btn.png');
    this.load.image('a', 'assets/elements/skills/slamIcon.png');
    this.load.image('b', 'assets/elements/skills/hookIcon.png');
    this.load.image('c', 'assets/elements/skills/bearTrapIcon.png');
    */
    /* game objects */
    /*
    this.load.image('bullet', 'assets/game-objects/box.png');
    this.load.image('box', 'assets/game-objects/box.png');
    this.load.image('bearTrap', 'assets/game-objects/trap.png');
    this.load.image('hook', 'assets/game-objects/hook.png');
    this.load.image('rope', 'assets/game-objects/rope.png');
    this.load.image('soul', 'assets/game-objects/soul.png');
    */
    this.load.onLoadComplete.add(this.loadComplete, this)
  }
  create() {
    this.time.advancedTiming = true;
  }
  update() {
    if (this.ready === true) this.state.start('Main');
  }
  render() {
  }

  loadComplete() {
     this.ready = true;
  }

}
 
  