
export default class Boot extends Phaser.State {

  preload() {
    this.load.image('preloadbar', '/assets/imgs/loader.png');
  }
  create() {
    this.stage.backgroundColor = '#ffffff';
  }
  update() {
    this.state.start('Preload');
  }
}
